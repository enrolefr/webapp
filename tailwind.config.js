import colors from 'tailwindcss/colors';
import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            black: colors.black,
            white: colors.white,
            red: colors.red,
            orange: colors.orange,
            yellow: colors.yellow,
            green: colors.green,
            gray: colors.slate,
            primary: "#0C98EA", // bleu foncé
            secondary: "#B4E4FF",
            tertiary: "#80D0C7", // vert foncé
            lightblue: "#DBF2FF", // bleu clair
            mediumblue: "#B4E4FF",
            deepblue: "#0C98EA",
            lightgreen: "#CFF7F2", // vert clair
            mediumgreen: "#A3D6D0",
            deepgreen: "#80D0C7",
            lightgray: "#dfdedc",
            mediumgray: "#696969",
            darkgray: "#60605e",
            darkblue: "#024369",
            dark: "#01304B",
            indigo: {
                100: '#e6e8ff',
                300: '#b2b7ff',
                400: '#7886d7',
                500: '#6574cd',
                600: '#5661b3',
                800: '#2f365f',
                900: '#191e38',
            },
        },
        extend: {
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
            },
        },
    },

    plugins: [forms, typography],
};
