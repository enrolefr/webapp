import { mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderAttr, ssrInterpolate } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.js";
const TextareaInput_vue_vue_type_style_index_0_scoped_ead158ec_lang = "";
const _sfc_main = {
  inheritAttrs: false,
  props: {
    id: {
      type: String,
      default() {
        return `textarea-input`;
      }
    },
    error: String,
    label: String,
    modelValue: String
  },
  emits: ["update:modelValue"],
  methods: {
    focus() {
      this.$refs.input.focus();
    },
    select() {
      this.$refs.input.select();
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  let _temp0;
  _push(`<div${ssrRenderAttrs(mergeProps({
    class: _ctx.$attrs.class
  }, _attrs))} data-v-ead158ec>`);
  if ($props.label) {
    _push(`<label class="form-label"${ssrRenderAttr("for", $props.id)} data-v-ead158ec>${ssrInterpolate($props.label)}:</label>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<textarea${ssrRenderAttrs(_temp0 = mergeProps({
    id: $props.id,
    ref: "input"
  }, { ..._ctx.$attrs, class: null }, {
    class: ["form-textarea", { error: $props.error }],
    value: $props.modelValue,
    rows: "5"
  }), "textarea")} data-v-ead158ec>${ssrInterpolate("value" in _temp0 ? _temp0.value : "")}</textarea>`);
  if ($props.error) {
    _push(`<div class="form-error" data-v-ead158ec>${ssrInterpolate($props.error)}</div>`);
  } else {
    _push(`<!---->`);
  }
  _push(`</div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/Forms/TextareaInput.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const TextareaInput = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender], ["__scopeId", "data-v-ead158ec"]]);
export {
  TextareaInput as T
};
