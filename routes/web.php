<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\SerieController;
use App\Http\Controllers\StreamerController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\Admin\ManagementController;
use App\Http\Controllers\Admin\ContactsController;
use App\Http\Controllers\Admin\GamesController;
use App\Http\Controllers\Admin\GenresController;
use App\Http\Controllers\Admin\SuggestionsController;
use App\Http\Controllers\Admin\TagsController;
use App\Http\Controllers\Admin\WatchlistsController;
use App\Http\Controllers\Admin\SeriesController;
use App\Http\Controllers\Admin\UsersController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});*/
Route::get('/', [IndexController::class, 'index'])
    ->name('home');

Route::get('/actual-plays-jeux', [IndexController::class, 'jeux'])
    ->name('jeux');

Route::get('/actual-plays-series', [IndexController::class, 'series'])
    ->name('series');

Route::get('/actual-plays-streamers', [IndexController::class, 'streamers'])
    ->name('streamers');

Route::get('/actual-plays-genres', [IndexController::class, 'genres'])
    ->name('genres');

Route::get('/actual-plays-statistics', [IndexController::class, 'stats'])
    ->name('stats');

Route::get('/actual-plays-search', [IndexController::class, 'search'])
    ->name('search');

Route::get('/actual-plays-agenda', [IndexController::class, 'agenda'])
    ->name('agenda');

Route::get('/jeux-de-roles-actual-plays/{jeu}', [GameController::class, 'findOne'])
    ->name('jeu');

Route::get('/jeux-de-roles-actual-plays-series/{serie}', [SerieController::class, 'findOne'])
->name('serie');

Route::get('/actual-plays-streamers-createurs-de-contenu-video/{streamer}', [StreamerController::class, 'findOne'])
->name('streamer');

Route::get('/actual-plays-genre/{genre}', [GenreController::class, 'findOne'])
->name('genre');

Route::get('/partenaires-actual-plays', [IndexController::class, 'partners'])
    ->name('partners');

Route::get('/contacter-enrolefr-communication', [IndexController::class, 'contact'])
    ->name('contact');

Route::get('/faq-actual-plays-enrolefr', [IndexController::class, 'faq'])
    ->name('faq');

Route::get('/conditions-mentions-legales-actual-plays-enrolefr', [IndexController::class, 'legal'])
    ->name('legal');


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {

    // Dashboard
    Route::get('/dashboard', [SuggestionsController::class, 'index'])->name('dashboard');
    Route::get('/management', [ManagementController::class, 'index'])->name('management');
    Route::get('/collection', [IndexController::class, 'collection'])->name('collection');

   // Users
    Route::get('manage/users', [UsersController::class, 'index'])->name('manage.users');
    Route::get('manage/users/create', [UsersController::class, 'create'])->name('manage.users.create');
    Route::post('manage/users', [UsersController::class, 'store'])->name('manage.users.store');
    Route::get('manage/users/{user}/edit', [UsersController::class, 'edit'])->name('manage.users.edit');
    Route::put('manage/users/{user}', [UsersController::class, 'update'])->name('manage.users.update');
    Route::delete('manage/users/{user}', [UsersController::class, 'destroy'])->name('manage.users.destroy');

    Route::put('manage/users/{user}/restore', [UsersController::class, 'restore'])
        ->name('manage.users.restore');

    // manage.users.suggestions.store    
    Route::post('manage/users/{user}/detach/{suggestion}', [UsersController::class, 'detachSuggestion'])
        ->name('manage.users.detach_suggestion');
    Route::post('manage/users/{user}/suggestions/store', [UsersController::class, 'createSuggestion'])
        ->name('manage.users.create_suggestion');    

    // Jeux
    Route::get('manage/jeux', [GamesController::class, 'index'])
        ->name('manage.jeux');

    Route::get('manage/jeux/{jeu}/edit', [GamesController::class, 'edit'])
        ->name('manage.jeux.edit');

    Route::get('manage/jeux/create', [GamesController::class, 'create'])
        ->name('manage.jeux.create');

    Route::post('manage/jeux', [GamesController::class, 'store'])
        ->name('manage.jeux.store');

    Route::put('manage/jeux/{jeu}', [GamesController::class, 'update'])
        ->name('manage.jeux.update');

    Route::delete('manage/jeux/{jeu}', [GamesController::class, 'destroy'])
        ->name('manage.jeux.destroy');

    Route::post('manage/jeux/{jeu}/attach/{suggestion}', [GamesController::class, 'attachSuggestion'])
        ->name('manage.jeux.attach_suggestion');

    Route::post('manage/jeux/{jeu}/detach/{suggestion}', [GamesController::class, 'detachSuggestion'])
        ->name('manage.jeux.detach_suggestion');

    // Genres
    Route::get('manage/genres', [GenresController::class, 'index'])
        ->name('manage.genres');

    Route::get('manage/genres/{genre}/edit', [GenresController::class, 'edit'])
        ->name('manage.genres.edit');

    Route::get('manage/genres/create', [GenresController::class, 'create'])
        ->name('manage.genres.create');

    Route::post('manage/genres', [GenresController::class, 'store'])
        ->name('manage.genres.store');

    Route::put('manage/genres/{genre}', [GenresController::class, 'update'])
        ->name('manage.genres.update');

    Route::delete('manage/genres/{genre}', [GenresController::class, 'destroy'])
        ->name('genres.destroy');


    // TagsController
    Route::get('manage/tags', [TagsController::class, 'index'])
        ->name('manage.tags');

    Route::get('manage/tags/{tag}/edit', [TagsController::class, 'edit'])
        ->name('manage.tags.edit');

    Route::get('manage/tags/create', [TagsController::class, 'create'])
        ->name('manage.tags.create');

    Route::post('manage/tags', [TagsController::class, 'store'])
        ->name('manage.tags.store');

    Route::put('manage/tags/{tag}', [TagsController::class, 'update'])
        ->name('manage.tags.update');

    Route::post('manage/tags/mass-edit', [TagsController::class, 'massEdit'])
    ->name('manage.tags.mass_edit');

    Route::delete('manage/tags/{tag}', [TagsController::class, 'destroy'])
        ->name('manage.tags.destroy');

    // WatchlistsController
    Route::get('manage/watchlists', [WatchlistsController::class, 'index'])
        ->name('manage.watchlists');

    Route::get('manage/watchlists/{watchlist}/edit', [WatchlistsController::class, 'edit'])
        ->name('manage.watchlists.edit');

    Route::get('manage/watchlists/create', [WatchlistsController::class, 'create'])
        ->name('manage.watchlists.create');

    Route::post('manage/watchlists', [WatchlistsController::class, 'store'])
        ->name('manage.watchlists.store');

    Route::put('manage/watchlists/{watchlist}', [WatchlistsController::class, 'update'])
        ->name('manage.watchlists.update');

    Route::delete('manage/watchlists/{watchlist}', [WatchlistsController::class, 'destroy'])
        ->name('manage.watchlists.destroy');

    // SeriesController
    Route::get('manage/series', [SeriesController::class, 'index'])
    ->name('manage.series');

    Route::get('manage/series/{serie}/edit', [SeriesController::class, 'edit'])
        ->name('manage.series.edit');

    Route::get('manage/series/create', [SeriesController::class, 'create'])
        ->name('manage.series.create');

    Route::post('manage/series', [SeriesController::class, 'store'])
        ->name('manage.series.store');

    Route::put('manage/series/{serie}', [SeriesController::class, 'update'])
        ->name('manage.series.update');

    Route::delete('manage/series/{serie}', [SeriesController::class, 'destroy'])
        ->name('manage.series.destroy');
    
    Route::post('manage/series/mass-delete', [SeriesController::class, 'massDelete'])
        ->name('manage.series.mass_delete');

        Route::post('manage/series/{serie}/attach/{suggestion}', [SeriesController::class, 'attachSuggestion'])
        ->name('manage.series.attach_suggestion');

    Route::post('manage/series/{serie}/detach/{suggestion}', [SeriesController::class, 'detachSuggestion'])
        ->name('manage.series.detach_suggestion');    
        //manage/series/${props.serie.id}/suggestions/${id}

    // Suggestions
    Route::get('manage/suggestions', [SuggestionsController::class, 'index'])
        ->name('manage.suggestions');

    Route::get('manage/suggestions/{suggestion}/edit', [SuggestionsController::class, 'edit'])
        ->name('manage.suggestions.edit');

    Route::get('manage/suggestions/create', [SuggestionsController::class, 'create'])
        ->name('manage.suggestions.create');

    Route::post('manage/suggestions', [SuggestionsController::class, 'store'])
        ->name('manage.suggestions.store');

    Route::post('manage/suggestions/mass-edit', [SuggestionsController::class, 'massEdit'])
        ->name('manage.suggestions.mass_edit');

    Route::put('manage/suggestions/{suggestion}', [SuggestionsController::class, 'update'])
        ->name('manage.suggestions.update');

    Route::delete('manage/suggestions/{suggestion}', [SuggestionsController::class, 'destroy'])
        ->name('manage.suggestions.destroy');

});
