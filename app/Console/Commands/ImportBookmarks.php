<?php

namespace App\Console\Commands;

use App\Models\Suggestion;
use Illuminate\Console\Command;
use Carbon\Carbon;

class ImportBookmarks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-bookmarks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'take a html bookmarks file, and import links to suggestions';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        //
        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTMLFile(resource_path() ."/import/bookmarks.html");
        $links = $doc->getElementsByTagName('a');

        $oldLinks = 0;
        $newLinks = 0;

        foreach ($links as $link) {
            // on cherche si le lien existe
            $suggestedLink = Suggestion::firstWhere('url', $link->getAttribute('href'));
            if ($suggestedLink instanceof \App\Models\Suggestion){
                $oldLinks++;
            } else {
                Suggestion::create([
                    'title' => $link->nodeValue,
                    "url" => $link->getAttribute('href'),
                    "user_id" => 2,
                    "created_at" => Carbon::today()
                ]);

                // echo $link->getAttribute('href'), PHP_EOL;
                // echo $link->nodeValue, PHP_EOL;

                $newLinks++;
            }
        }
        echo " ajouts ". $newLinks.", ".$oldLinks. " ignorés", PHP_EOL;

    }
}
