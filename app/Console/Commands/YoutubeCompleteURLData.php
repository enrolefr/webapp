<?php

namespace App\Console\Commands;

use App\Models\Suggestion;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Alaouy\Youtube\Facades\Youtube;
use Illuminate\Support\Str;

class YoutubeCompleteURLData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'youtube:complete-url-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call Youtube API to get back url data';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $suggestions = Suggestion::pluck('url')->toArray();
        $youtubeIds = array_map(function ($a) {
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $a, $matches, PREG_OFFSET_CAPTURE);
            if(count($matches) == 2){
                // echo $matches[1][0], PHP_EOL;
                return $matches[1][0];
            }
            return false;
        }, $suggestions);
        $offset = 0;

        while (count(array_slice($youtubeIds, $offset,50)) > 0){
            echo $offset, PHP_EOL;

            $videoList = Youtube::getVideoInfo(array_slice($youtubeIds, $offset,50));

            foreach($videoList as $video){
                // gestion de la vignette
                $selected = Suggestion::where('url', 'LIKE', '%v='.$video->id.'%')
                    ->whereNull('imageUrl')
                    ->first();

                /*if ($selected instanceof \App\Models\Suggestion && $selected != null){
                    $selected->imageUrl = $video->snippet->thumbnails->medium->url;
                    $selected->save();
                }*/
                // mise à jour des paramètres
                $selected2 = Suggestion::where('url', 'LIKE', '%v='.$video->id.'%')
                    ->first();
                if ($selected2 instanceof \App\Models\Suggestion && $selected2 != null){
                    echo $selected2->title, PHP_EOL;
                    $selected2->description = $video->snippet->description;
                    $selected2->views = $video->statistics->viewCount;
                    $selected2->likes = $video->statistics->likeCount;
                    $selected2->favorites = $video->statistics->favoriteCount;
                    $selected2->comments = $video->statistics->commentCount;
                    $selected2->imageUrl = $video->snippet->thumbnails->medium->url;


                    // gestion des tags
                    if (!empty($video->snippet->tags)){
                        $tagIds = array();
                        foreach($video->snippet->tags as $tag){
                            $checkTag = \App\Models\Tag::where('slug','=',Str::slug($tag))->first();
                            if ($checkTag instanceof \App\Models\Tag){
                                array_push($tagIds, $checkTag->id);
                            } else {
                                $newTag = \App\Models\Tag::factory()->create(['name' => $tag, 'active' => false]);
                                array_push($tagIds, $newTag->id);
                            }
                        }
                        $selected2->tags()->sync($tagIds);
                    }
                    $selected2->update([ "created_at" => Carbon::today()]);
                    $selected2->save();
                }
            }

            $offset += 50;
        }
        \Log::info('command youtube:completeURLData is executed');
    }

}
