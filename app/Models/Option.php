<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Option extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = [
        'label',
        'slug',
    ];

    public function setLabelAttribute($value)
    {
        $this->attributes['label'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_options', 'user_id', 'option_id');
    }
}
