<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tag extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name', 'active'
    ];

    // protected $with = ['suggestions'];

    public function suggestions()
    {
        return $this->belongsToMany(Suggestion::class,  'suggestions_tags', 'tag_id', 'suggestion_id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
}
