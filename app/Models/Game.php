<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Suggestion;
use App\Models\Serie;

class Game extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'slug'
    ];

    // protected $with = ['suggestions','series'];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    public function suggestions()
    {
        return $this->hasMany(Suggestion::class);
    }

    public function series()
    {
        return $this->hasMany(Serie::class);
    }
}
