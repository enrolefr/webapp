<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Watchlist extends Model
{
    use HasFactory;
    protected $keyType = 'string';

    protected $fillable = [
        'name',
        'slug',
        'user_id',
        'order',
    ];

    public function suggestions()
    {
        return $this
            ->belongsToMany(Suggestion::class,  'suggestions_watchlists', 'watchlist_id', 'suggestion_id')
            ->with('game');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    public function setUserIdAttribute($value)
    {
        $watchlist_user = User::find($value);
        $user_watchlists_nb = count($watchlist_user->watchlists);
        $this->attributes['order'] = ++$user_watchlists_nb;
        $this->attributes['user_id'] = (int)$value;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
