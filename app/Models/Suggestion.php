<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Suggestion extends Model
{
    use HasFactory;
    protected $keyType = 'string';
    protected $fillable = [
        'title',
        'eztitle',
        'url',
        'imageUrl',
        'params',
        'description',
        'views',
        'likes',
        'comments',
        'favorites',
        'user_id',
        'game_id',
    ];

    protected $with = ['user', 'game', 'genres', 'tags', 'series'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'validated' => 'boolean',
        'validated_at' => 'datetime',
    ];

    public function setIdAttribute($id)
    {
        if (empty($id)) {
            $this->attributes['id'] = Str::uuid();
        }
    }

    /*public function getParamsAttribute($params)
    {
        if (empty($params)) {
            $decodedParams = json_decode($params);
            $stats = new \stdClass;
            if (isset($decodedParams->viewCount)) $stats->views = $decodedParams->viewCount;
            if (isset($decodedParams->likeCount)) $stats->likes = $decodedParams->likeCount;
            if (isset($decodedParams->commentCount)) $stats->comments = $decodedParams->commentCount;
            if (isset($decodedParams->favoriteCount)) $stats->fav = $decodedParams->favoriteCount;
            $this->attributes['stats'] = $stats;
            if (isset($decodedParams->tags)) $this->attributes['tags'] = $decodedParams->tags;
            $this->attributes['description'] = $decodedParams->description;
        }
    }*/

    public function user()
    {
        return $this->belongsTo(User::class,  'user_id', 'id');
    }

    public function game()
    {
        return $this->belongsTo(Game::class, 'game_id', 'id');
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'suggestions_genres', 'suggestion_id', 'genre_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'suggestions_tags', 'suggestion_id', 'tag_id');
    }
    public function activeTags()
    {
        return $this->belongsToMany(Tag::class, 'suggestions_tags', 'suggestion_id', 'tag_id')->where('active',true)->limit(25);
    }

    public function watchlists()
    {
        return $this->belongsToMany(Watchlist::class, 'suggestions_watchlists', 'suggestion_id', 'watchlist_id');
    }

    public function series()
    {
        return $this->belongsToMany(Serie::class, 'suggestions_series', 'suggestion_id', 'serie_id');
    }
}
