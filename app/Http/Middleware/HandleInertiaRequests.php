<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     */
    public function version(Request $request): ?string
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @return array<string, mixed>
     */
    public function share(Request $request): array
    {
        return [
            ...parent::share($request),
            'ziggy' => fn () => [
                ...(new Ziggy)->toArray(),
                'location' => $request->url(),
            ],
            'config' => [
                'app' => [
                    'name' => config('app.name'),
                    'url' => config('app.url'),
                    'asset_url' => config('app.asset_url'),
                    'debug' => config('app.debug'),
                    'timezone' => config('app.timezone'),
                    'locale' => config('app.locale'),
                    'fallback_locale' => config('app.fallback_locale'),
                    'yt_sugg' => config('app.yt_sugg')
                ],
                'auth' => [
                    'guards' => config('auth.guards'),
                    'passwords' => config('auth.passwords'),
                ],
                'broadcasting' => [
                    'connections' => config('broadcasting.connections'),
                ],
            ]
        ];
    }
}
