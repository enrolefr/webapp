<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Models\Game;
use App\Models\Serie;
use App\Models\User;
use App\Models\Suggestion;
use Inertia\Inertia;

class GameController extends Controller
{

    public function findOne($j)
    {
        $game = Game::with('series')->where('slug', $j)->firstOrFail();
        $suggestions = Suggestion::where('game_id', $game->id)
            ->with('game', 'genres','tags', 'user')
            ->paginate(24);

        // Ajouter les suggestions à la série du jeu
        foreach ($game->series as $serie) {
            $serieWithSuggestions = Serie::where('id', $serie->id)
                ->with('suggestions')
                ->first();
            $serie->suggestions = $serieWithSuggestions->suggestions;
        }


        return Inertia::render('Game', [
            // 'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'game' => $game,
            'suggestions' => $suggestions,
        ]);
    }


}
