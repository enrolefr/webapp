<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Models\Suggestion;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tag::with('suggestions')
        ->orderBy('active', 'desc')
        ->withCount('suggestions')
        ->orderBy('suggestions_count', 'desc')
        ->paginate(50);

        return Inertia::render('Tags/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'tags' => $tags,
        ]);
    }

    public function create()
    {
        return Inertia::render('Tags/Create', []);
    }

    public function store()
    {
        $tag = Tag::create(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50']
            ])
        );

        return Redirect::route('manage.tags')->with('success', 'Tag créé.');
    }
    public function edit(Tag $tag)
    {
        return Inertia::render('Tags/Edit', [
            'tag' => [
                'id' => $tag->id,
                'name' => $tag->name,
                'active' => (bool)$tag->active,
            ]
        ]);
    }

    public function update(Tag $tag)
    {       

        $tag->update(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50'],
                'active' => ['boolean']
            ])
        );
        

        return Redirect::route('manage.tags')->with('success', 'Tag mis à jour.');
    }

    public function massEdit(Request $request)
    {
        $tag_ids = $request->input('tag_ids');
        $massEdit = $request->input('mass_edit');

        switch($massEdit){
            case 'activate':
                Tag::whereIn('id', $tag_ids)->update(['active' => true]);
                break;
            case 'disactivate'  :
                Tag::whereIn('id', $tag_ids)->update(['active' => false]);
                break;
            case 'delete':
                // detach suggestions tags first to avoid foreign key constraint
                Suggestion::with('tags')->whereIn('id', $tag_ids)->each(function($suggestion){
                    $suggestion->tags()->detach();
                });
                Tag::whereIn('id', $tag_ids)->delete();
                break;    
        }
    
        return Redirect::route('manage.tags')->with('success', 'Tags supprimés.');
    }

    public function destroy(Tag $tag)
    {
        $tag->delete();

        return Redirect::route('manage.tags')->with('success', 'Tag supprimé.');
    }
}
