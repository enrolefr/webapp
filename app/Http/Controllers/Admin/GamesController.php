<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Suggestion;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class GamesController extends Controller
{

    public function index()
    {
        $games = Game::with('suggestions')->paginate(25);

        return Inertia::render('Games/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'games' => $games,
        ]);
    }

    public function create()
    {
        return Inertia::render('Games/Create', []);
    }

    public function store()
    {
        Game::create(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50']
            ])
        );

        return Redirect::route('manage.jeux')->with('success', 'Jeu créé.');
    }
    public function edit(Game $jeu)
    {
        // search for all suggestions with game name inside the title
        $suggestions = Suggestion::where('title', 'like', '%' . $jeu->name . '%')->get();
        // remove suggestions already attached to the game
        foreach ($jeu->suggestions as $suggestion) {
            $suggestions = $suggestions->reject(function ($value, $key) use ($suggestion) {
                return $value->id == $suggestion->id;
            });
        }
        
        return Inertia::render('Games/Edit', [
            'game' => [
                'id' => $jeu->id,
                'slug' => $jeu->slug,
                'name' => $jeu->name,
                'suggestions' => $jeu->suggestions, // Ajoutez cette ligne
                'updated_at' => Carbon::now()
            ],
            'suggestions' => $suggestions
        ]);
    }

    public function update(Game $jeu)
    {
       
        $jeu->update(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50']
            ])
        );

        return Redirect::route('manage.jeux')->with('success', 'Jeu mis à jour.');
    }

    public function attachSuggestion(Game $jeu, Suggestion $suggestion)
    {
        // Mettre à jour la propriété game_id de la suggestion
        $suggestion->game_id = $jeu->id;
        $suggestion->save();

        // on retourne sur la page de l'édition des jeux
        return  Redirect::route('manage.jeux.edit', ['jeu' => $jeu->id])->with('success', 'Suggestion attachée.');
    }

    public function detachSuggestion(Game $jeu, Suggestion $suggestion)
    {
        // Mettre à jour la propriété game_id de la suggestion
        $suggestion->game_id = null;
        $suggestion->save();

        // on retourne sur la page de l'édition des jeux
        return  Redirect::route('manage.jeux.edit', ['jeu' => $jeu->id])->with('success', 'Suggestion détachée.');
    }

    public function destroy(Game $jeu)
    {
         // Mettre à jour la propriété game_id des suggestions
        foreach ($jeu->suggestions as $suggestion) {
            $suggestion->game_id = null; 
            $suggestion->save();
        }
        // Supprimer le jeu
        $jeu->delete();

        // on retourne sur la page de gestion des jeux
        return Redirect::route('manage.jeux')->with('success', 'Jeu supprimé.');
    }
}
