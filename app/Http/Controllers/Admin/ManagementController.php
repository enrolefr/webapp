<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Suggestion;
use App\Models\Game;
use App\Models\User;
use Inertia\Inertia;

class ManagementController extends Controller
{
    public function index()
    {
        $suggestions = Suggestion::with('game', 'genres','activeTags')->paginate(50);
        return Inertia::render('Management', [
            'suggestions' =>  $suggestions,
            'games' => Game::orderBy('name')->get()->toArray(),
            'users' => User::where('organization',1)->orderBy('last_name')->pluck('id', 'last_name')->toArray()
        ]);
    }
}
