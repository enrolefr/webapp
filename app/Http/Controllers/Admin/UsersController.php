<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Team;
use App\Models\Suggestion;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Alaouy\Youtube\Facades\Youtube;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function index()
    {
        return Inertia::render('Users', [
            'filters' => Request::all('search', 'role', 'trashed'),
            'users' => Auth::user()->with('teams', 'parent')
                ->get()
                ->transform(fn ($user) => [
                    'id' => $user->id,
                    'name' => ($user->organization == true) ? $user->last_name : $user->first_name . ' ' . $user->last_name,
                    'email' => $user->email,
                    'owner' => $user->owner,
                    'photo' => $user->photo_path ? URL::route('image', ['path' => $user->photo_path, 'w' => 40, 'h' => 40, 'fit' => 'crop']) : null,
                    'deleted_at' => $user->deleted_at,
                    'organization' => ($user->organization) ? 'Oui' : 'Non',
                    'parent' => ($user->parent_id) ? $user->parent->last_name : '',
                    'teams' => $user->teams->map->only('id', 'name'),
                ]),
            'teams' => Team::all(),    
        ]);
    }

    public function create()
    {
        $organizations = User::where('organization', true)->get();

        return Inertia::render('Users/Create', [
            'teams' => Team::all()->map(function ($team) {
                return ['value' => $team->id, 'label' => $team->name];
            })->toArray(),
            'organizations' => $organizations
        ]);
    }

    public function store()
    {
        $data = \Illuminate\Support\Facades\Request::validate([
            'first_name' => ['nullable', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'description' => ['nullable'],
            'email' => ['required', 'email', 'max:50'],
            'organization' => ['nullable','boolean'],
            'current_team_id' => ['nullable','integer', 'between:1,10'],
            'profilePhotoPath' => ['nullable'],
            'parent_id' => ['nullable', 'integer', 'exists:users,id'],
            // 'image' => ['nullable'],
            'main_channel' => ['max:50']
        ]);
        $data['uuid'] = (string) Str::uuid();
        if (!isset($data['organization'])) {
            $data['organization'] = false;
        } 

        if (!empty($data['password'])){
            $data['password'] = bcrypt($data['password']);
        } else {
            // generate a random password
            $data['password'] = bcrypt(Str::random(10));
        }
        
        $user = User::create($data);
        $user->save();

        $data = \Illuminate\Support\Facades\Request::all();
        if (isset($data['team_ids'])){
            $user->teams()->sync($data['team_ids']);
        }
       
        return Redirect::route('manage.users')->with('success', 'Utilisateur créé.');
    }

    public function edit(User $user)
    {
        $videoList = array();

        // if main_channel is not null, we try to get the channel list from Youtube
        if (!empty($user->main_channel) && config('app.yt_sugg')){            
            try {

                $params = [
                    'q'            => 'actual play',
                    'type'         => 'video',
                    'channelId'    =>  $user->main_channel,
                    'part'         => 'id, snippet',
                    'regionCode'   => 'FR',
                    'maxResults'   => 100,
                    'relevanceLanguage' => 'fr',
                 ];
        
                $videoSearch = Youtube::searchAdvanced($params);

                // $returnedVideoList = Youtube::listChannelVideos($user->main_channel,60);

                $cnt = 0;
                foreach ($videoSearch as $kv => $video){
                    // we checked if there is already a suggestion with the same url
                    $suggestion = Suggestion::where('url', 'LIKE', 'https://www.youtube.com/watch?v='.$video->id->videoId.'%')->first();

                    if ($suggestion == null){
                        $videoList[] = $video;
                        $cnt++;
                    }
                }
            } catch (\Exception $e) {
                // $user->main_channel = null;
                // $user->save();  
            }    
        } 

        $organizations = User::where('organization', true)->get();
        
        return Inertia::render('Users/Edit', [
            'user' => [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'owner' => $user->owner,
                'photo' => $user->photo_path ? URL::route('image', ['path' => $user->photo_path, 'w' => 60, 'h' => 60, 'fit' => 'crop']) : null,
                'deleted_at' => $user->deleted_at,
                'current_team_id' => $user->current_team_id,
                'teams' => $user->teams,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
                'description' => $user->description,
                'main_channel' => $user->main_channel,
                'organization' => (bool)$user->organization,
                'parent_id' => $user->parent_id,
                'suggestions' => $user->suggestions,
            ],
            'teams' => Team::all()->map(function ($team) {
                return ['value' => $team->id, 'label' => $team->name];
            })->toArray(),
            'organizations' => $organizations,
            'videoList' => $videoList,
        ]);
    }

    public function update(User $user)
    {   
        $form_data = \Illuminate\Support\Facades\Request::validate([
            'first_name' => ['max:50'],
            'last_name' => ['required', 'max:50'],
            'description' => ['nullable'],
            'organization' => ['boolean'],
            'current_team_id' => ['nullable','integer', 'between:1,10'],
            'email' => ['required', 'email'],
            'profilePhotoPath' => ['nullable'],
            'parent_id' => ['nullable', 'integer'],
            // 'image' => ['nullable'],
            'main_channel' => ['max:50']
        ]);
        $user->update($form_data);
        
        $data = \Illuminate\Support\Facades\Request::all();
        if (isset($data['team_ids'])){
            $user->teams()->sync($data['team_ids']);
            $user->current_team_id = $data['team_ids'][0];
        }
        $user->save();

        return Redirect::route('manage.users')->with('success', 'Utilisateur mis à jour.');
    }

    // detach suggestion
    public function detachSuggestion(User $user, $suggestion)
    {
        // for all suggestions, set the user_id to null 
        $user->suggestions()    
            ->where('id', $suggestion)
            ->update(['user_id' => null]);

        return Redirect::back()->with('success', 'Suggestion détachée.');
    }

    // create suggestion
    public function createSuggestion(User $user)
    {
        // we checked if there is already a suggestion with the same url
        $suggestion = Suggestion::where('url', Request::input('url'))->first();
        if ($suggestion != null){
            return response()->json(['message' => 'Url already exists'], 422);
        }
        
        $data = \Illuminate\Support\Facades\Request::validate([
            'title' => ['required', 'max:255'],
            'url' => ['required'],
            'validated_at' => ['date'],
            'validated' => ['boolean']
        ]);
        $data['user_id'] = $user->id;
        $suggestion = Suggestion::create($data);

        //$suggestion->save();
        
        return Redirect::route('manage.users.edit',['user' => $user->id])->with('success', 'Suggestion créée.');
    }

    public function massEdit()
    {
        $data = \Illuminate\Support\Facades\Request::all();
        if(!empty($data['user_ids']) && !empty($data['action']) && !empty($data['value'])){
            if ($data['action'] == 'delete-checked'){
                User::whereIn('id', $data['user_ids'])->delete();
            }
        }

        return Redirect::back()->with('success', 'Utilisateurs mis à jour.');
    }

    public function createImage(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
          if ($image->isValid()){     
                $imageName = time().'-'.$image->getClientOriginalName();
                $destinationPath = public_path('/images/users');
                $image->move($destinationPath, $imageName);
        
                if (!empty($request->input('user'))){
                    $user = User::find($request->input('user'));
                    $user->update(['imgPath' => '/images/users/' . $imageName]);
                    $user->save();
                }
            }
        }
        
        return Redirect::route('manage.users')->with('success', 'Utilisateur mis à jour.');
    }

    public function destroy(User $user)
    {
        if (App::environment('demo') && $user->isDemoUser()) {
            return Redirect::back()->with('error', 'Deleting the demo user is not allowed.');
        }

        $user->delete();

        return Redirect::back()->with('success', 'Utilisateur supprimé.');
    }

    public function restore(User $user)
    {
        $user->restore();

        return Redirect::back()->with('success', 'Utilisateur restauré.');
    }
}
