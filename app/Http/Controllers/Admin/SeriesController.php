<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Serie;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SeriesController extends Controller
{
    public function index()
    {
        $series = Serie::with('user', 'game', 'suggestions')->paginate(10);
        $jeux = \App\Models\Game::all()->pluck('name','id')->toArray();
        $users = \App\Models\User::all()->pluck('last_name','id')->toArray();

        return Inertia::render('Series/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'series' => $series,
            'jeux' => $jeux,
            'users' => $users
        ]);
    }

    public function create()
    {
        $users = \App\Models\User::all()->map(function ($user) {
            return ['value' => $user->id, 'label' => $user->last_name];
        })->toArray();
        $jeux = \App\Models\Game::all()->map(function ($game) {
            return ['value' => $game->id, 'label' => $game->name];
        })->toArray();
        
        return Inertia::render('Series/Create', [
            'usersList' => $users,
            'gamesList' => $jeux
        ]);
    }

    public function store()
    {
        Serie::create(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50'],
                'user_id' => ['required','numeric'],
                'game_id' => ['required','numeric']
            ])
        );

        return Redirect::route('manage.series')->with('success', 'Serie créée.');
    }
    public function edit(Serie $serie)
    {
        $users = \App\Models\User::all()->map(function ($user) {
            return ['value' => $user->id, 'label' => $user->last_name];
        })->toArray();
        $jeux = \App\Models\Game::all()->map(function ($game) {
            return ['value' => $game->id, 'label' => $game->name];
        })->toArray();
        $suggestions = \App\Models\Suggestion::where('game_id', $serie->game_id)->get();
        return Inertia::render('Series/Edit', [
            'serie' => [
                'id' => $serie->id,
                'name' => $serie->name,
                'description' => $serie->description,
                'imageUrl' => $serie->imageUrl,
                'user_id' => $serie->user_id,
                'game_id' => $serie->game_id,
                'total_views' => $serie->total_views,
                'total_likes' => $serie->total_likes,
                'suggestions' => $serie->suggestions,
                'updated_at' => Carbon::now()
            ],
            'usersList' => $users,
            'gamesList' => $jeux,
            'suggestions' => $suggestions
        ]);
    }

    public function update(Serie $serie)
    {
        $serie->update(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50'],
                'user_id' => ['required','numeric'],
                'game_id' => ['required','numeric']
            ])
        );
        $serie->total_views = $this->countSuggestionViews($serie);
        $serie->total_likes = $this->countSuggestionLikes($serie);
        $serie->save();
        
        return Redirect::route('manage.series')->with('success', 'Série mise à jour.');
    }

    // attach suggestion to serie
    public function attachSuggestion(Serie $serie, \App\Models\Suggestion $suggestion)
    {
        $serie->suggestions()->attach($suggestion->id);
        $serie->total_views = $this->countSuggestionViews($serie);
        $serie->total_likes = $this->countSuggestionLikes($serie);
        $serie->save();

        return Redirect::route('manage.series.edit', ['serie' => $serie->id])->with('success', 'Suggestion ajoutée.');
    }

    // detach suggestion from serie 
    public function detachSuggestion(Serie $serie, \App\Models\Suggestion $suggestion)
    {
        $serie->suggestions()->detach($suggestion->id);
        $serie->total_views = $this->countSuggestionViews($serie);
        $serie->total_likes = $this->countSuggestionLikes($serie);
        $serie->save();
        
        return Redirect::route('manage.series.edit', ['serie' => $serie->id])->with('success', 'Suggestion supprimée.');
    }


    public function massDelete(Request $request)
    {
        $serie_ids = $request->input('serie_ids');

        Serie::whereIn('id', $serie_ids)->delete();
    
        return Redirect::route('manage.series')->with('success', 'Séries supprimées.');
    }


    public function destroy(Serie $serie)
    {
        $serie->delete();

        return Redirect::route('manage.series')->with('success', 'Serie supprimée.');
    }
    
    // internal method to count all suggestion views in a serie
    private function countSuggestionViews($serie)
    {
        $suggestions = $serie->suggestions;
        $views = 0;
        foreach ($suggestions as $suggestion) {
            $views += $suggestion->views;
        }
        return $views;
    }

    // internal method to count all suggestion likes in a serie
    private function countSuggestionLikes($serie)
    {
        $suggestions = $serie->suggestions;
        $likes = 0;
        foreach ($suggestions as $suggestion) {
            $likes += $suggestion->likes;
        }
        return $likes;
    }
}
