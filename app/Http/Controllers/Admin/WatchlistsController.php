<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Watchlist;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class WatchlistsController extends Controller
{
    public function index()
    {
        $watchlists = Watchlist::with('user', 'suggestions')->paginate(10);

        return Inertia::render('Watchlists/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'watchlists' => $watchlists,
        ]);
    }

    public function create()
    {
        $users = \App\Models\User::all()->pluck('email','id')->toArray();
        return Inertia::render('Watchlists/Create', [
            'usersList' => $users
        ]);
    }

    public function store()
    {
        Watchlist::create(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50'],
                'order' => ['required','numeric'],
                'user_id' => ['required','numeric']
            ])
        );

        return Redirect::route('manage.watchlists')->with('success', 'Watchlist créée.');
    }
    public function edit(Watchlist $watchlist)
    {
        $users = \App\Models\User::all()->pluck('email','id')->toArray();
        return Inertia::render('Watchlists/Edit', [
            'watchlist' => [
                'id' => $watchlist->id,
                'order' => $watchlist->order,
                'name' => $watchlist->name,
                'user_id' => $watchlist->user_id,
                'updated_at' => Carbon::now()
            ],
            'usersList' => $users
        ]);
    }

    public function update(Watchlist $watchlist)
    {
        $watchlist->update(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50'],
                'order' => ['required','numeric'],
                'user_id' => ['required','numeric']
            ])
        );

        return Redirect::back()->with('success', 'Watchlist mise à jour.');
    }

    public function destroy(Watchlist $watchlist)
    {
        $watchlist->delete();

        return Redirect::route('manage.watchlists')->with('success', 'Watchlist supprimée.');
    }
}
