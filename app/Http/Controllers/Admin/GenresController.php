<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Genre;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class GenresController extends Controller
{
    public function index()
    {
        $genres = Genre::with('suggestions')
        ->withCount('suggestions')
        ->orderBy('suggestions_count', 'desc')
        ->paginate(10);

        return Inertia::render('Genres/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'genres' => $genres,
        ]);
    }

    public function create()
    {
        return Inertia::render('Genres/Create', []);
    }

    public function store()
    {
        Genre::create(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50']
            ])
        );

        return Redirect::route('manage/genres')->with('success', 'Genre créé.');
    }
    public function edit(Genre $genre)
    {
        return Inertia::render('Genres/Edit', [
            'genre' => [
                'id' => $genre->id,
                'slug' => $genre->slug,
                'name' => $genre->name,
                'updated_at' => Carbon::now()
            ]
        ]);
    }

    public function update(Genre $genre)
    {
        $genre->update(
            \Illuminate\Support\Facades\Request::validate([
                'name' => ['required', 'max:50']
            ])
        );

        return Redirect::route('manage.genres')->with('success', 'Genre mis à jour.');
    }

    public function destroy(Genre $genre)
    {
        $genre->delete();

        return Redirect::route('manage.genres')->with('success', 'Genre supprimé.');
    }
}
