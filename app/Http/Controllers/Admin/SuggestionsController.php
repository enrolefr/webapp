<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Suggestion;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Alaouy\Youtube\Facades\Youtube;

class SuggestionsController extends Controller
{
    public function index()
    {
        $filters = Request::all('orderby', 'order');
        $suggestions = Suggestion::with('game', 'genres','tags', 'user')
        ->orderBy($filters['orderby'] ?? 'updated_at', $filters['order'] ?? 'desc')
        ->paginate(50);
        $streamers = User::whereHas('teams', function($query){
                $query->where('team_id', 3);
            })
            ->where('organization',1)->orderBy('last_name')->pluck('last_name', 'id')->toArray();
        return Inertia::render('Suggestions/Dashboard', [
            'suggestions' =>  $suggestions,
            'games' => Game::orderBy('name')->get()->toArray(),
            'streamers' => $streamers,
            'genres' => \App\Models\Genre::orderBy('name')->get()->toArray(),
            'filters' => $filters,
        ]);
    }


    public function create()
    {
        if (config('app.yt_sugg')){
            // récupération des vidéos youtube
            $params = [
                'q'            => 'actual play',
                'type'         => 'video',
                'part'         => 'id, snippet',
                'regionCode'   => 'FR',
                'maxResults'   => 100,
                'relevanceLanguage' => 'fr',
            ];

            $videoSearch = Youtube::searchAdvanced($params);
            shuffle($videoSearch);
            $videoSearch = array_slice($videoSearch, 0, 10);
            $videoIds = array_map(function($object) {
                return $object->id->videoId;
            }, $videoSearch);
            $videoList = Youtube::getLocalizedVideoInfo($videoIds, 'fr');
        } else {
            $videoList = array();
        }
        
        // streamers
        $streamers = User::whereHas('suggestions')
        ->whereHas('teams', function($query){
            $query->where('team_id', 3);
        })
        ->where('organization',1)
        ->orderBy('last_name')
        ->get()
        ->pluck('last_name', 'id')
        ->toArray();


        return Inertia::render('Suggestions/Create', [
            'games' => \App\Models\Game::all(),
            'genres' => \App\Models\Genre::all(),
            'tags' => \App\Models\Tag::all(),
            'streamers' => $streamers,
            'youtubeList' => $videoList
        ]);
    }

    public function store()
    {
        $suggestion = Suggestion::create(
            \Illuminate\Support\Facades\Request::validate([
                'title' => ['required', 'max:250'],
                'eztitle' => ['nullable', 'max:250'],
                'url' => ['required', 'max:255'],
                'imageUrl' => ['nullable', 'max:255'],
                'params' => ['nullable'],
                'validated' => ['nullable'],
                'game_id' => ['required'],
                'user_id' => ['numeric','nullable']
            ])
        );

        return Redirect::route('dashboard')->with('success', 'Suggestion créée.');
    }
    public function edit(Suggestion $suggestion)
    {
          // streamers
          $streamers = User::whereHas('suggestions')
          ->whereHas('teams', function($query){
              $query->where('team_id', 3);
          })
          ->where('organization',1)
          ->orderBy('last_name')
          ->get()
          ->map(function ($user) {
            return ['value' => $user->id, 'label' => $user->last_name];
            })
          ->toArray();
          // dd($streamers);

        return Inertia::render('Suggestions/Edit', [
            'suggestion' => [
                'id' => $suggestion->id,
                'title' => $suggestion->title,
                'eztitle' => $suggestion->eztitle,
                'url' => $suggestion->url,
                'imageUrl' => $suggestion->imageUrl,
                'params' => $suggestion->params,
                'validated' => $suggestion->validated,
                'game_id' => $suggestion->game_id,
                'user_id' => $suggestion->user_id,
                'views' => $suggestion->views,
                'likes' => $suggestion->likes,
                'comments' => $suggestion->comments,
                'favorites' => $suggestion->favorites,
                'updated_at' => Carbon::now(),
                'genre_ids' => $suggestion->genres->pluck('id')->toArray(),
                'tag_ids' => $suggestion->tags->pluck('id')->toArray(),
            ],
            'games' => \App\Models\Game::all(),
            'genres' => \App\Models\Genre::all(),
            'tags' => \App\Models\Tag::all(),
            'streamers' => $streamers,
        ]);
    }

    public function massEdit()
    {
        $data = \Illuminate\Support\Facades\Request::all();

        if(!empty($data['suggestion_ids']) && !empty($data['actions'])){
            foreach($data['actions'] as $item){
                if ($item['action'] == 'game'){
                    Suggestion::whereIn('id', $data['suggestion_ids'])->update(['game_id' => $item['value']]);
                }
                if ($item['action'] == 'streamer'){
                    Suggestion::whereIn('id', $data['suggestion_ids'])->update(['user_id' => $item['value']]);
                }
                if ($item['action'] == 'genre'){
                    $selectedSuggestions = Suggestion::whereIn('id', $data['suggestion_ids'])->get();
                    foreach ($selectedSuggestions as $ss) {
                        $ss->genres()->sync($item['value']);
                    }
                }
                if ($item['action'] == 'delete'){
                    Suggestion::whereIn('id', $data['suggestion_ids'])->delete();
                }     
            }
        }

        return Redirect::route('dashboard', ['page' => $data['page']])->withInput()->with('success', 'Suggestions mises à jour.');
    }

    public function update(Suggestion $suggestion)
    {
        $data = \Illuminate\Support\Facades\Request::validate([
            'title' => ['required', 'max:250'],
            'eztitle' => ['nullable', 'max:250'],
            'url' => ['required', 'max:255'],
            'imageUrl' => ['nullable', 'max:255'],
            'params' => ['nullable'],
            'validated' => ['nullable'],
            'game_id' => ['required'],
            'user_id' => ['numeric','nullable']
        ]);
        $suggestion->update($data);
        
        $data = \Illuminate\Support\Facades\Request::all();
        if (isset($data['genre_ids'])){
            $suggestion->genres()->sync($data['genre_ids']);
        }
        if (isset($data['tag_ids'])){
            $suggestion->tags()->sync($data['tag_ids']);
        }
        $suggestion->save();

        return Redirect::route('dashboard')->withInput()->with('success', 'Suggestion mise à jour.');
    }

    public function destroy(Suggestion $suggestion)
    {
        $suggestion->delete();

        return Redirect::route('dashboard')->withInput()->with('success', 'Suggestion supprimée.');
    }
}
