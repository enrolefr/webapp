<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Game;
use App\Models\Suggestion;
use Validator;
use App\Http\Resources\SuggestionResource;
use Illuminate\Http\JsonResponse;
class SuggestionsController extends BaseController
{
    /**
     * Sections a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $products = Suggestion::all();
        return $this->sendResponse(SuggestionResource::collection($products), 'Suggestions retrieved successfully.');
    }

    /**
     * Sections a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listByGame($gameSlug): JsonResponse
    {
        $game = Game::where('slug', $gameSlug)->first();
        if (isset($game->id)){
            $series = Suggestion::where('game_id', $game->id)->get();
            return $this->sendResponse(SuggestionResource::collection($series), 'Suggestions retrieved successfully.');
        } else {
            return $this->sendResponse(null, 'No serie retrieved successfullly');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'detail' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $product = Suggestion::create($input);
        return $this->sendResponse(new SuggestionResource($product), 'Suggestion created successfully.');
    }
    /**
     * Sections the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id): JsonResponse
    {
        $product = Suggestion::find($id);
        if (is_null($product)) {
            return $this->sendError('Suggestion not found.');
        }
        return $this->sendResponse(new SuggestionResource($product), 'Suggestion retrieved successfully.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suggestion $product): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'detail' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $product->name = $input['name'];
        $product->detail = $input['detail'];
        $product->save();
        return $this->sendResponse(new SuggestionResource($product), 'Suggestion updated successfully.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suggestion $product): JsonResponse
    {
        $product->delete();
        return $this->sendResponse([], 'Suggestion deleted successfully.');
    }
}
