<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Game;
use App\Models\Serie;
use Validator;
use App\Http\Resources\SerieResource;
use Illuminate\Http\JsonResponse;
class SeriesController extends BaseController
{
    /**
     * Sections a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $products = Serie::all();
        return $this->sendResponse(SerieResource::collection($products), 'Series retrieved successfully.');
    }

    /**
     * Sections a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listByGame($gameSlug): JsonResponse
    {
        $game = Game::where('slug', $gameSlug)->first();
        if (isset($game->id)){
            $series = Serie::where('game_id', $game->id)->get();
            return $this->sendResponse(SerieResource::collection($series), 'Series retrieved successfully.');
        } else {
            return $this->sendResponse(null, 'No serie retrieved successfullly');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'detail' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $product = Serie::create($input);
        return $this->sendResponse(new SerieResource($product), 'Serie created successfully.');
    }
    /**
     * Sections the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id): JsonResponse
    {
        $product = Serie::find($id);
        if (is_null($product)) {
            return $this->sendError('Serie not found.');
        }
        return $this->sendResponse(new SerieResource($product), 'Serie retrieved successfully.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Serie $product): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'detail' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $product->name = $input['name'];
        $product->detail = $input['detail'];
        $product->save();
        return $this->sendResponse(new SerieResource($product), 'Serie updated successfully.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Serie $product): JsonResponse
    {
        $product->delete();
        return $this->sendResponse([], 'Serie deleted successfully.');
    }
}
