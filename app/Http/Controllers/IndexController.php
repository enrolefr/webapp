<?php

namespace App\Http\Controllers;

use App\Models\Suggestion;
use App\Models\Game;
use App\Models\User;
use App\Models\Watchlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $pagination_count = config('view.pagination_count');
        $suggestions = Suggestion::with('game', 'user', 'genres','activeTags')
            ->when($request->query('game_id'), function($query, $game_id){
                $query->where('game_id', $game_id);
            })
            ->when($request->query('streamer_id'), function($query, $streamer_id){
                $query->where('user_id', $streamer_id);
            })
            ->paginate($pagination_count)
            ->withQueryString();

        $suggestions->getCollection()->transform(function ($value) {
            if (is_array($value->active_tags)){
                $value->active_tags = array_slice($value->active_tags,0,4);
            }
           
            return $value;
        });

        foreach($suggestions as $suggestion){
            if (!empty($suggestion->user)){
                $suggestion->user->childs = User::where('parent_id', $suggestion->user->id)->get();
            }
        }
        

        $series = \App\Models\Serie::with('user', 'game', 'suggestions')
            ->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();

        $games = Game::whereHas('suggestions')
            ->orderBy('name')
            ->get();

        $streamers = User::whereHas('suggestions')
            ->whereHas('teams', function($query){
                $query->where('team_id', 3);
            })
            ->where('organization',1)
            ->orderBy('last_name')
            ->get()
            ->pluck('last_name', 'id')
            ->toArray();

        return Inertia::render('Index', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'suggestions' =>  $suggestions,
            "games" => $games,
            "streamers" => $streamers,
            "series" => $series,
            "filters" => $request->only(['game_id', 'streamer_id'])
        ]);
    }

    public function collection()
    {
        $user = Auth::user();
        $myWatchlists = null;
        if ($user !== null){
            $myWatchlists = Watchlist::where('user_id', $user->id)->pluck('name', 'id')->toArray();
        }
        return Inertia::render('Collection', [
            'games' => Game::paginate(10),
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'watchlist' => Watchlist::with('suggestions')->first(),
            'myWatchlists' => $myWatchlists,
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }

    public function search()
    {
        return Inertia::render('Search', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }


    public function jeux()
    {
        return Inertia::render('Jeux', [
            'jeux' =>  \App\Models\Game::with('suggestions')->orderBy('name')->paginate(40),
        ]);
    }


    public function series()
    {
        return Inertia::render('Series', [
            'series' =>  \App\Models\Serie::with('game', 'user','suggestions')->orderBy('updated_at')->paginate(40),
        ]);
    }

    public function genres()
    {
        return Inertia::render('Genres', [
            'genres' =>  \App\Models\Genre::with('suggestions')->paginate(40),
        ]);
    }

    public function streamers()
    {
        $streamers = User::with('suggestions')
        ->whereHas('suggestions')
        ->whereHas('teams', function($query){
            $query->where('team_id', 3);
        })
        ->where('organization',1)
        ->orderBy('last_name')
        ->paginate(40);
        
        return Inertia::render('Streamers', [
            'streamers' =>  $streamers,
        ]);
    }


    public function stats(Request $request)
    {
        $suggestions = Suggestion::with('game', 'user', 'genres','activeTags')
        ->when($request->query('game_id'), function($query, $game_id){
            $query->where('game_id', $game_id);
        })
        ->when($request->query('streamer_id'), function($query, $streamer_id){
            $query->where('user_id', $streamer_id);
        })
        ->get();

        $games = Game::whereHas('suggestions')
            ->orderBy('name')
            ->get();

        $streamers = User::whereHas('suggestions')
            ->whereHas('teams', function($query){
                $query->where('team_id', 3);
            })
            ->where('organization',1)
            ->orderBy('last_name')
            ->get()
            ->pluck('last_name', 'id')
            ->toArray();

        return Inertia::render('Stats', [
            'suggestions' => $suggestions,
            "games" => $games,
            "streamers" => $streamers,
            "filters" => $request->only(['game_id', 'streamer_id'])
        ]);
    }

    public function agenda()
    {
        return Inertia::render('Agenda', [
            'filters' => \Illuminate\Support\Facades\Request::all('agenda', 'trashed'),
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }

    public function contact()
    {
        return Inertia::render('Contact', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }

    public function legal()
    {
        return Inertia::render('Legal', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }

    public function partners()
    {
        return Inertia::render('Partners', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }

    public function faq()
    {
        return Inertia::render('FAQ', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }

    public function settings()
    {
        return Inertia::render('Logged/Settings', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'suggestions' =>  Suggestion::with('game', 'genres','tags')->paginate(10),
        ]);
    }
}
