<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Models\Serie;
use App\Models\Suggestion;
use Inertia\Inertia;

class SerieController extends Controller
{

    public function findOne($s)
    {
        $serie = Serie::with('suggestions', 'game', 'user')->where('slug', $s)->firstOrFail();
        
        return Inertia::render('Serie', [
            // 'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'serie' => $serie
        ]);
    }


}
