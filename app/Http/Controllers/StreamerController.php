<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use App\Models\Suggestion;
use Inertia\Inertia;

class StreamerController extends Controller
{

    public function findOne($s)
    {
        
        $streamer = User::with('suggestions', 'series')->where('id', $s)->firstOrFail();
        
          // Ajouter les suggestions à la série du jeu
          foreach ($streamer->series as $serie) {
            $serieWithSuggestions = \App\Models\Serie::where('id', $serie->id)
                ->with('suggestions','game', 'user')
                ->first();
            $serie->suggestions = $serieWithSuggestions->suggestions;
            $serie->game = $serieWithSuggestions->game;
            $serie->user = $serieWithSuggestions->user;
        }

        return Inertia::render('Streamer', [
            // 'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'streamer' => $streamer
        ]);
    }


}
