<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Models\Genre;
use App\Models\Suggestion;
use Inertia\Inertia;

class GenreController extends Controller
{

    public function findOne($s)
    {
        $genre = Genre::with('suggestions')->where('slug', $s)->firstOrFail();
        return Inertia::render('Genre', [
            // 'filters' => \Illuminate\Support\Facades\Request::all('search', 'trashed'),
            'genre' => $genre
        ]);
    }


}
