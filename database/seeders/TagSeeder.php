<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Tag::factory()
            ->create(['name' => "Explication de règles"])
            ->create(['name' => "Solo"])
            ->create(['name' => "Un Joueur/un MJ"])
            ->create(['name' => "A plusieurs Sans MJ"])
            ->create(['name' => "non-mixte"])
            ->create(['name' => "LGBTIA+"])
            ->create(['name' => "épisode"])
            ->create(['name' => "Grand format"]);
    }
}
