<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\Partner;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Contact::factory()->create(['first_name' => 'contact',
            'last_name' => 'test',
            'email' => 'toto@toto.com',
            'phone' => "+33612345678",
            'country' => 'FR',
            'function' => 'community manager',
            'user_id' => 1
        ]);
    }
}
