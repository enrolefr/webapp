<?php

namespace Database\Seeders;

use App\Models\Game;
use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // ajouter des jeux
        Game::factory()
            ->create(['name' => "City of Mist"])
            ->create(['name' => "Tales from the loop"])
            ->create(['name' => "Cyberpunk"])
            ->create(['name' => "7ème mer"])
            ->create(['name' => "NOC"])
            ->create(['name' => "Hunter le jugement"])
            ->create(['name' => "Fallout"])
            ->create(['name' => "Masks"])
            ->create(['name' => "Monsterhearts"])
            ->create(['name' => "Loup-Garou"])
            ->create(['name' => "Gods"])
            ->create(['name' => "Batman JDR"])
            ->create(['name' => "Dune"])
            ->create(['name' => "Zombie d100"])
            ->create(['name' => "Fabula Ultima"])
            ->create(['name' => "Vermine 2047"])
            ->create(['name' => "Brancalonia"])
            ->create(['name' => "Degenesis"])
            ->create(['name' => "Knight"])
            ->create(['name' => "Trinités"])
            ->create(['name' => "Monster of the Week"])
            ->create(['name' => "Les lames du cardinal"])
            ->create(['name' => "Hexagon"]);



    }
}
