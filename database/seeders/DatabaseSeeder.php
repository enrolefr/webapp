<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Partner;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $account = Account::create(['name' => 'EnRole (FR)']);

        /*
        User::factory(5)->create(['account_id' => $account->id]);
        */

        $this->call([
            UserSeeder::class,
            ContactSeeder::class,
            GameSeeder::class,
            TagSeeder::class,
            GenreSeeder::class,
            SuggestionSeeder::class
        ]);


        Artisan::call("app:import-bookmarks");
        Artisan::call("youtube:complete-url-data");
    }
}
