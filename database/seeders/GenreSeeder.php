<?php

namespace Database\Seeders;

use App\Models\Genre;
use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        Genre::factory()
            ->create(['name' => "Fantasy"])
            ->create(['name' => "Science-Fiction"])
            ->create(['name' => "Horreur/Fantastique"])
            ->create(['name' => "Cyberpunk"])
            ->create(['name' => "Atompunk"])
            ->create(['name' => "Steampunk"])
            ->create(['name' => "Post-Apocalyptique"])
            ->create(['name' => "Historique"])
            ->create(['name' => "Préhistorique"])
            ->create(['name' => "Antique"])
            ->create(['name' => "Contemporain"])
            ->create(['name' => "Solarpunk"]);


    }
}
