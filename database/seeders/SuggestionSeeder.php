<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\Game;
use App\Models\Serie;
use App\Models\Suggestion;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class SuggestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // ajout de Daredevil
        Suggestion::factory()
            ->create([
                'id' => Str::uuid(),
                'validated' => true,
                'validated_at' => Carbon::now(),
                'title' => "[JDR Solo] Daredevil et City of Mist | Episode 1 : jouer avec l'enfer",
                "url" => 'https://www.youtube.com/watch?v=lKHMtseksU0',
                "imageUrl" => 'https://i.ytimg.com/vi/lKHMtseksU0/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLCR24kDQHw53MHWD_EBX5pE_kz7Dw',
                'user_id' => 2,
                'contact_id' => Contact::first()->id,
                'game_id' => Game::first()->id
            ])
            ->create([
                'id' => Str::uuid(),
                'validated' => true,
                'validated_at' => Carbon::now(),
                'title' => "[JDR Solo] Daredevil et City of Mist | Episode 2 : Ceux qui reviennent",
                "url" => 'https://www.youtube.com/watch?v=BqOLFl2_cfk',
                "imageUrl" => 'https://i.ytimg.com/vi/BqOLFl2_cfk/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLC0ywc6_yC883i60gJqE7gZ3TZymg',
                'user_id' => 2,
                'contact_id' => Contact::first()->id,
                'game_id' => Game::first()->id
            ]);

        // ajout des dents de Millers square

        $millersSquare = Serie::factory()
            ->create([
                'name' => "Les dents de Miller's Square",
                "url" => 'https://www.youtube.com/playlist?list=PLwNOaLDwHonFNmXopVcWI4_JXWEj5bDqa',
                "game_id" => 4
            ]);

        Suggestion::factory()
            ->create([
                'id' => Str::uuid(),
                'validated' => false,
                'validated_at' => Carbon::now(),
                'title' => "Les dents de Miller's Square épisode 1, avec @CriticalPlayjdr",
                "url" => 'https://www.youtube.com/watch?v=3KQqv1-AMCg&list=PLwNOaLDwHonG8_2tV5_LT9A7C1HnGQ25N&index=4',
                "imageUrl" => 'https://i.ytimg.com/vi/3KQqv1-AMCg/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLB-iczFvu-pS6LNQxOqgE8jJ6eGOw',
                'user_id' => 2,
                'game_id' => Game::first()->id
            ])
            ->create([
                'id' => Str::uuid(),
                'validated' => false,
                'validated_at' => Carbon::now(),
                'title' => "Les dents de Miller's Square épisode 2, avec @CriticalPlayjdr",
                "url" => 'https://www.youtube.com/watch?v=T5I-u16GvTc&list=PLwNOaLDwHonG8_2tV5_LT9A7C1HnGQ25N&index=1',
                "imageUrl" => 'https://i.ytimg.com/vi/T5I-u16GvTc/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLCaEAQFl_6YX02A4I1fO8ydNKkw1w',
                'user_id' => 2,
                'game_id' => Game::first()->id
            ]);
        $millersSquare1 = Suggestion::where('title', 'LIKE', "Les dents de Miller's Square%")->first();
        $millersSquare2 = Suggestion::where('title', 'LIKE', "Les dents de Miller's Square%")->skip(1)->first();
        $millersSquare->suggestions()->sync([$millersSquare1->id, $millersSquare2->id]);
    }
}
