<?php

namespace Database\Seeders;

use App\Models\Genre;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        User::factory()->create([
            'first_name' => 'François-Xavier',
            'last_name' => 'Guillois',
            'email' => 'internet@fxguillois.email',
            'password' => '1terM1n4bl3!',
        ]);

        User::factory()->create([
            'first_name' => 'LAS',
            'last_name' => 'League of Anonymous Streamers',
            'organization' => true,
            'email' => 'gutsandgrit@gmail.com',
            'password' => '1terM1n4bl3!'
        ]);

        User::factory()->create([
            'first_name' => 'DS',
            'last_name' => 'Dice Story',
            'organization' => true,
            'email' => 'dicestory@fxguillois.mozmail.com',
            'password' => 'changeme'
        ]);

        User::factory()->create([
            'first_name' => 'VV',
            'last_name' => 'Vltima Verba',
            'organization' => true,
            'email' => 'vltimaverba@fxguillois.mozmail.com',
            'password' => 'changeme'
        ]);

        User::factory()->create([
            'first_name' => 'Ceizyl',
            'last_name' => 'Ceizyl',
            'organization' => true,
            'email' => 'ceizyl@fxguillois.mozmail.com',
            'password' => 'changeme'
        ]);

        User::factory()->create([
            'first_name' => 'Virgile',
            'last_name' => 'Virgile JDR',
            'organization' => true,
            'email' => 'virgile@fxguillois.mozmail.com',
            'password' => 'changeme'
        ]);

        \App\Models\Team::factory()->create([
            'name' => "Admins",
            'user_id' => 1,
            'personal_team' => true,
        ]);

        \App\Models\Team::factory()->create([
            'name' => "Modérateurs",
            'user_id' => 1,
            'personal_team' => false,
        ]);

        $streamers = \App\Models\Team::factory()->create([
            'name' => "Streameurs",
            'user_id' => 1,
            'personal_team' => false,
        ]);

        $streamers->users()->sync([2,3,4,5,6]);

        \App\Models\Team::factory()->create([
            'name' => "Members",
            'user_id' => 1,
            'personal_team' => false,
        ]);
    }
}
