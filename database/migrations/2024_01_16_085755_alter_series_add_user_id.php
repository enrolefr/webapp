<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('series', function (Blueprint $table) {
            $table->string('user_id');
            $table->text('description')->nullable();
            $table->string('total_views')->default(0);
            $table->string('total_likes')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('series', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('description');
            $table->dropColumn('total_views');
            $table->dropColumn('total_likes');
            $table->dropColumn('updated_at');
            $table->dropColumn('created_at');
        });
    }
};
