<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuggestionsSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestions_series', function (Blueprint $table) {
            $table->id();
            $table->uuid('suggestion_id');
            $table->unsignedBiginteger('serie_id');
            
            $table->foreign('suggestion_id')->references('id')
                ->on('suggestions')->onDelete('cascade');
            $table->foreign('serie_id')->references('id')
                ->on('series')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggestions_series');
    }
}
