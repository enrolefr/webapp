<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuggestionsWatchlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestions_watchlists', function (Blueprint $table) {
            $table->id();
            $table->uuid('suggestion_id');
            $table->uuid('watchlist_id');

            $table->foreign('suggestion_id')->references('id')
                ->on('suggestions')->onDelete('cascade');
            $table->foreign('watchlist_id')->references('id')
                ->on('watchlists')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggestions_watchlists');
    }
}
