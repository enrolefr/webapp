<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SerieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $game = \App\Models\Game::inRandomOrder()->first();
        $user = \App\Models\User::inRandomOrder()->first();

        return [
            'name' => $this->faker->name,
            'game_id' => $game->id,
            'user_id' => $user->id,
        ];
    }
}
