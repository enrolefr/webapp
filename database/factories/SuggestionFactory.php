<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SuggestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->uuid,
            'title' => $this->faker->name,
            'url' =>  $this->faker->url,
            'imageUrl' =>  $this->faker->url,
            'description' => "",
            'views' => 0,
            'likes' => 0,
            'comments' => 0,
            'favorites' => 0,
            'validated' => false,
            'validated_at' => null,
            'params' => json_encode(['foo' => 'bar']),
            'user_id' => 1,
            'game_id' => null,
            'contact_id' => null,
        ];
    }
}
