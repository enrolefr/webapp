{
    "domain": "Cabinet-partage.fr",
    "main_title": "Notre mission",
    "main_description": "Fournir aux professionnels de santé et de bien-être un lieu de consultation au prix juste, adapté au remplissage de leur agenda",
    "nb_therapeutes": 6,
    "nb_happy": 240,
    "therapeutes_title": "Thérapeutes",
    "therapeutes_description": "Quelque soit leurs disponibilités, nos thérapeutes ont pour seul objectif votre santé et votre bien-être.",
    "therapeutes_note": "Certains thérapeutes ont un créneau horaire établi pour chaque semaine.",
    "therapeutes_warning": "Chaque thérapeute possède sa propre patientèle. Merci de le contacter directement si vous souhaitez un rendez-vous.",
    "chambery_chapo": "Le cabinet de soins de chambéry rassemble des soins infirmiers, un suivi physique ou psychique de chaque patient. Prenez contact avec nos thérapeutes.",
    "vallieres_chapo": "La maison de santé de Vallières est en cours de construction. Vous êtes thérapeute ? Vous cherchez une salle pour donner un cours ou un séminaire ? Contactez-nous.",
    "newsletter_title": "Restez informé(e)",
    "newsletter_description": "Nous vous enverrons des informations sur les professionnels qui sont disponibles dans nos centres partagés.",
    "copyright": "Copyright © 2022 SCI la Tapisserie. Tous droits réservés.",
    "available_durations": [
        { "id": 10, "label": "1h", "active": false },
        { "id": 20, "label": "2h", "active": true },
        { "id": 30, "label": "demi-journée", "active": true },
        { "id": 40, "label": "journée", "active": true }
    ]
}
