// resources/js/helpers.js

export const sortByNameAsc = (arr) => {
    return arr.slice().sort((a, b) => {
    if (a.name < b.name) {
        return -1;
    }
    if (a.name > b.name) {
        return 1;
    }
    return 0;
    });
}

export const sortByNameDesc = (arr) => {
    return arr.slice().sort((a, b) => {
    if (a.name > b.name) {
        return -1;
    }
    if (a.name < b.name) {
        return 1;
    }
    return 0;
    });
}