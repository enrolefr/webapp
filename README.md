# Gestion des suggestions

* app:import-bookmarks : importe les bookmarks depuis un fichier de liens
* youtube:complete-url-data : se connecte à Youtube pour mettre à jour les données


# Ping CRM

A demo application to illustrate how Inertia.js works.
![](https://raw.githubusercontent.com/inertiajs/pingcrm/master/screenshot.png)



Run database migrations:

```sh
php artisan migrate:fresh --seed 
```

Run the dev server (the output will give the address):

```sh
php artisan serve
```

## Running tests

To run the Ping CRM tests, run:

```
phpunit
```
